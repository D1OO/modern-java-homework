package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger shieldHPCap;

	private PositiveInteger hullHP;

	private PositiveInteger hullHPCap;

	private PositiveInteger capacitorCap;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.shieldHPCap = shieldHP;
		this.hullHP = hullHP;
		this.hullHPCap = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorCap = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public static CombatReadyShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		if (name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new CombatReadyShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, speed, size,
				attackSubsystem, defenciveSubsystem);
	}

	@Override
	public void endTurn() {
		int newCapacitorAmount = this.capacitorAmount.value() + this.capacitorRechargeRate.value();
		if (newCapacitorAmount > this.capacitorCap.value()) {
			newCapacitorAmount = this.capacitorCap.value();
		}
		this.capacitorAmount = new PositiveInteger(newCapacitorAmount);

	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		AttackAction attackAction = null;
		if (this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			attackAction = new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem);
			this.capacitorAmount = new PositiveInteger(
					this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
		}
		return Optional.ofNullable(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackResult attackResult = null;
		attack = this.defenciveSubsystem.reduceDamage(attack);
		int newShieldHP = this.shieldHP.value() - attack.damage.value();
		int newHullHP = this.hullHP.value();
		if (newShieldHP <= 0) {
			newHullHP += newShieldHP;
			if (newHullHP <= 0) {
				newHullHP = 0;
				attackResult = new AttackResult.Destroyed();
			}
			newShieldHP = 0;
		}
		this.shieldHP = new PositiveInteger(newShieldHP);
		this.hullHP = new PositiveInteger(newHullHP);
		if (attackResult == null) {
			attackResult = new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
		}
		return attackResult;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		RegenerateAction regenerateAction = null;
		int newCapacitorAmount = this.capacitorAmount.value()
				- this.defenciveSubsystem.getCapacitorConsumption().value();

		if (newCapacitorAmount >= 0) {
			regenerateAction = this.defenciveSubsystem.regenerate();
			PositiveInteger hullHPRegenerated = regenerateAction.hullHPRegenerated;
			PositiveInteger shieldHPRegenerated = regenerateAction.shieldHPRegenerated;

			this.hullHP = new PositiveInteger(this.hullHP.value() + regenerateAction.hullHPRegenerated.value());
			if (this.hullHP.value() > this.hullHPCap.value()) {
				hullHPRegenerated = new PositiveInteger(
						regenerateAction.hullHPRegenerated.value() - (this.hullHP.value() - this.hullHPCap.value()));
				this.hullHP = new PositiveInteger(this.hullHPCap.value());
			}

			this.shieldHP = new PositiveInteger(this.shieldHP.value() + regenerateAction.shieldHPRegenerated.value());
			if (this.shieldHP.value() > this.shieldHPCap.value()) {
				shieldHPRegenerated = new PositiveInteger(regenerateAction.shieldHPRegenerated.value()
						- (this.shieldHP.value() - this.shieldHPCap.value()));
				this.shieldHP = new PositiveInteger(this.shieldHPCap.value());
			}

			regenerateAction = new RegenerateAction(shieldHPRegenerated, hullHPRegenerated);
			this.capacitorAmount = new PositiveInteger(newCapacitorAmount);
		}
		return Optional.ofNullable(regenerateAction);

	}

}
