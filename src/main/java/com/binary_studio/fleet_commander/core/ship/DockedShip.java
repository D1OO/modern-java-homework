package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		if (name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}
		else {
			int newTotalPGConsumption = subsystem.getPowerGridConsumption().value();
			if (this.defenciveSubsystem != null) {
				newTotalPGConsumption += this.defenciveSubsystem.getPowerGridConsumption().value();
			}

			if (newTotalPGConsumption > this.powergridOutput.value()) {
				throw new InsufficientPowergridException(newTotalPGConsumption - this.powergridOutput.value());
			}
			else {
				this.attackSubsystem = subsystem;
			}
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}
		else {
			int newTotalPGConsumption = subsystem.getPowerGridConsumption().value();
			if (this.attackSubsystem != null) {
				newTotalPGConsumption += this.attackSubsystem.getPowerGridConsumption().value();
			}

			if (newTotalPGConsumption > this.powergridOutput.value()) {
				throw new InsufficientPowergridException(newTotalPGConsumption - this.powergridOutput.value());
			}
			else {
				this.defenciveSubsystem = subsystem;
			}
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return CombatReadyShipBuilder.named(this.name).shield(this.shieldHP.value()).hull(this.hullHP.value())
				.speed(this.speed.value()).capacitor(this.capacitorAmount.value())
				.capacitorRegen(this.capacitorRechargeRate.value()).size(this.size.value())
				.attackingSubsystem(this.attackSubsystem).defensiveSubsystem(this.defenciveSubsystem).construct();
	}

}
