package com.binary_studio.tree_max_depth;

import java.util.*;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	/**
	 * Tree Depth-first Search, <a href=
	 * "https://www.baeldung.com/java-depth-first-search#2-inorder-traversal">Iterative
	 * preorder traversal</a>
	 * @param rootDepartment
	 * @return Integer - Hierarchy max depth
	 */
	public static Integer calculateMaxDepth(Department rootDepartment) {
		Department current;
		int counter;
		Stack<Map.Entry<Department, Integer>> stack = new Stack<>();
		Set<Integer> depths = new HashSet<>();

		if (rootDepartment != null) {
			stack.push(Map.entry(rootDepartment, 0));
		}
		while (!stack.isEmpty()) {
			Map.Entry<Department, Integer> e = stack.pop();
			current = e.getKey();
			counter = e.getValue();
			if (current != null) {
				counter++;
				List<Department> subDepartments = current.subDepartments;
				if (!subDepartments.isEmpty() && checkContainsNotNullDepartment(subDepartments)) {
					depths.add(counter);
					for (Department d : subDepartments) {
						if (d != null) {
							stack.push(Map.entry(d, counter));
						}
					}
				}
				else {
					depths.add(counter);
				}
			}
		}

		return depths.stream().max(Integer::compareTo).orElse(0);
	}

	private static boolean checkContainsNotNullDepartment(List<Department> subDepartments) {
		for (Department d : subDepartments) {
			if (d != null) {
				return true;
			}
		}
		return false;
	}

}
